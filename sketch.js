const branch_0_market = [
	
]

const branch_0_barracks = [

]


const branch_0 = [
	"//great evil",
	"'a great evil?? you must go and tell the knights immediately!'",
	"'wasn't there a knight here just a moment ago?'",
	"",
	"you head outside.",
	{
		"go to the market": branch_0_market,
		"go to the palace": [
			"you go over to the desk and ask the lady for an audience with the king.",
			"lady: 'why would his highness agree for an audience with a filthy peasant such as yourself?'",
			"'i spit on you!'",
			"*spitting sounds*",
			{
				"i came to warn this kingdom of a great evil!": [
					"'a great evil you say?'",
					"'his highness' prophet did mention that there would be a messenger bearing dire news.'",
					"'come with me, his highness shall see you right away'",
					"led by the lady you traverse corridor after corridor, through a maze which has no dead ends.",
					"before you stands a large ornate door.",
					"'right this way.'",
					"the large door swung itself open upon your arrival, revealing a large room, stained with sunlight coming through decorated windows.",
					"in front of you, not far, but definitely out of reach, was the throne,",
					"a large seat, built into the very room itself,",
					"its intricate designs continuing into the walls,",
					"truly a room fit for a king.",
					"on the throne sat the king, elevated above all others.",
					"on either side of the king stood guards wielding long spears.",
					{
						"kneel down before the king and tell him about the great evil": [
							"king: 'i see. so it is true.'",
							"'beatris, prepare to mobilise our troops!'",
							"'and you, adventurer, i give you the title of \"hero\".'",
							"'fare thee well, our hero.'",
							"the end."
						],
						"stare the king in the eyes and tell him of the great evil": [
							"lady: 'how disrespectful! kneel before his highness!'",
							"king: 'it is fine, sabrina, i do not care for such things as \"nobles' manners\".'",
							"'tell me, adventurer, of this great evil.'",
							"",
							"your eyes turn a fiery red,",
							"from your feet burn tall frames, climbing up to the ceiling.",
							{
								"'i am the great evil!'": [
									"in an instant, your face gets as close as half a span to the king's face.",
									"warm blood.",
									"red.",
									"it was just a myth, it seems.",
									"all humans bleed the same.",
									"you pull your hand back through the hole in the king's chest.",
									"you turn around and leave the room.",
									"",
									"'Oh, that is true, i suppose.'",
									"what!?",
									"'humans do all bleed the same,'",
									"'it's a shame i'm not a human...'",
									"you turn around to see a large horned figure towering over you.",
									"its fur-no its hair, purple, poorly masking his demonic skin.",
									"'it's going to take a bit more than that, Gehennom!'",
									"the tall demon lunges at you with superhuman speed.",
									"you dodge its claws only to be hit with its massive hooves.",
									{
										"'i should have known it was you, pride!'": [
											"flames engulf your hand, extending into claws.",
											{
												"'don't think i'm going to lose, though'": [
													"your flames turn a dark purple and cover your whole body.",
													"'ah, you've finally released your true form, gehennom...'",
													"'this is going to be a bit tricky, is it not?'",
													"in less then a tenth of a moment, two great evils clashed countless times.",
													"the shockwave produced from these clashes flattened the palace and shook the whole city.",
													"pride leaps up into the air before the ground below him collapsed.",
													"with only one leg you push yourself and spin in the air, moving towards him, finally hitting is jaw from below with a kick, sending pride flying even further up.",
													"'Ouch! that hurt, gehennom-what!?'",
													"you have already disappeared from where you kicked pride and appeared behind him.",
													"'This cannot be!'",
													"with one well placed blow from your fiery fist, you send pride into his grave, deep inside the earth.",
													{
														"'you have sinned. ask him for forgiveness.'": [
															"The end."
														]
													}
												]
											}
										]
									}
								]
							}
						]
					}
				],
				"ignore her and just stay in the castle for a while": [
					"lady: 'GUARDS!!'",
					"the guards drag you to the dungeon.",
					"",
					"",
					"you wake up tied to a chair in a dimly-lit room.",
					"",
					"",
					"hours pass,",
					"",
					"",
					"",
					"maybe even days.",
					"",
					"",
					"a creaking sound behind you informs you of the presence of a door.",
					"",
					"step",
					"step",
					"step",
					"",
					"???: 'well, then, what do we have here?'",
					"the source of the familiar voice walks in front of you.",
					"friedrich: 'i thought you were a good child. i must have thought wrong.'",
					"friedrich brandishes his jagged, rusty tool with a smile stretching from ear to ear.",
					"'oh! how long i've waited for someone to play with!'",
					"'but this toy, this one is for later.'",
					"friedrich walks behind you. all you can hear is the delicate clinging of metal instruments.",
					"",
					"clank clank",
					"",
					"friedrich walks in front of you.",
					"'shall we begin, then?'",
					"starting from the left pinky, he removes your fingernails one by one using a pair of rusty pliers.",
					"you scream until you cannot scream anymore, and then some.",
					"the cold air of the room stings your naked fingers.",
					"",
					"blood,",
					"",
					"your blood.",
					"",
					"'it is time.'",
					"friedrich grabs his rusty tool and starts to slowly carve out your stomach.",
					"even though nothing but your screams can be heard,",
					"you can still hear friedrich singing his lullaby.",
					"'there is no greater evil than us in this kingdom, foolish child!'",
					{
						"so... sleepy...": [
							"as you close your eyes you can feel the cold embrace of the dark.",
							"all your pain fades away.",
							"it is finally over.",
							"the end."
						],
						"'...wrong.'": [
							{
								"'i am the great evil.'": [
									"warmth.",
									"heat.",
									"flames surround your whole body, converging in front of you.",
									"you stand up,",
									"curl your fingers around his throat,",
									"wring his throat out like a wet cloth.",
									"",
									"the fires burn hot, eat through the stone walls.",
									{
										"'i warned you, but you ignored me.'": [
											{
												"'now all is ash'": [
													"the end."
												]
											}
										]
									}
								]
							}
						] 
					}],
				"leave and go to the barracks instead": branch_0_barracks,
				"leave and go to the market instead": branch_0_market
			}
		],
		"go to the barracks": branch_0_barracks
	}
]

const branch_1 = [
	"//legendary item"
]

const branch_2 = [
	"//monster"
]


const story = [
	"you wake up in a bed at an inn.",
	"there seems to be a ruckus downstairs.",
	{
		"find out what all this noise is about" : [
			"you go downstairs to find two large men fighing.",
			{
				"come closer": [
					"as you come closer you hear what they are fighting about.",
					"or more accurately, you feel a fist in your face.",
					"you wake up lying on two chairs with your face bleeding.",
					"???: 'ah, i see you're awake.'",
					"'i am friedrich, a knight of the order.'",
					"'you really got your face bashed in over there, be more careful next time.'",
					"'i must go now. let us know if you have any problems in the future.'",
					"friedrich exits through the front door.",
					"",
					"",
					"innkeeper: 'sorry that you got involved in that fight.'",
					"'have a free drink, on the house.'",
					"the inkeeper hands you a beer and sits down next to you.",
					"inkeeper: 'so, what did you come to the capital for? the people who stay at my inn are always looking for something in this city.'",
					"",
					"choose your fate",
					{
						"i came to warn this kingdom of a great evil": branch_0,
						"i came after hearing rumours of a legendary item hidden in this city": branch_1,
						"i came to hunt the monster lurking in the outskirts of this city": branch_2
					}

				]
			}
		]
	}
];


let queuedUpdates = [];

let queueOverride = [];

let updateWaitTime = 200;

let textSpeedModifier = 9;
let textSpeed = 18 + textSpeedModifier;
let printSize = 17;

let savePoint = [];

let unlockedChapters = {};

const alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

let paused = false;

let pausedUpdates;

let backgroundColour = [0x00, 0xFF, 0xFF];

let terminalOutputPane = new GameObject("terminalOutputPane", function() {
	noStroke();
	fill([0x5F, 0x5F, 0x5F]);
	rect(this.x, this.y, this.width, this.height);

	fill(0xFF);
});

let terminalOutput = new GameObject("terminalOutput", null, terminalOutputPane);

terminalOutput.height = 0;

let terminalInputPane = new GameObject("terminalInputPane", function() {
	noStroke();
	fill([0x9F, 0x9F, 0x9F]);
	rect(this.x, this.y, this.width, this.height);

	fill(0xFF);
});

let terminalArrow = new Text("terminalArrow", null, terminalInputPane);

terminalArrow.text = ">";
//terminalArrow.vertAlign = 'center';

let terminalText = new Text("terminalText", function() {
	textFont(this.font);
	textSize(this.height * 1.1);

	let text = this.text + terminalTextCompletion.text;

	this.size = this.height * 0.95;
	
	if (textWidth(text) > this.width) {
		textSize(this.size * 1.05);

		if (textWidth(text) > this.width) {

			let w = this.width;
			let ts = textWidth(text);
			let s = this.size;

			this.size *= (w / ts) * 0.95;

			console.log(this.size);
		}
	}

	terminalTextCompletion.size = this.size; 

	
}, terminalInputPane);

terminalText.vertAlign = 'center';

let terminalTextCompletion = new Text("terminalTextCompletion", function() {
	textSize(this.size);
	textFont(this.font);
	this.x = terminalText.x + textWidth(terminalText.text);
	this.y = terminalText.y;
	this.height = terminalText.height;
	this.width = terminalText.width + (terminalText.x - this.x);
}, terminalInputPane);

terminalTextCompletion.vertAlign = 'center';
terminalTextCompletion.fill = [190, 190, 190];

function printTerm(a, b, c, wait, override) {
	queue(function() {_printTerm(a, b, c); updateWaitTime += !wait ? a.length * textSpeed : 0;}, override);
}

function _printTerm(string, size, font) {
	textObj = new Text("string" + new Date().getTime(), null, terminalOutput);

	push();

    textObj.fill = [220, 220, 220];

	textObj.size = size || printSize;
	textObj.font = font || textObj.font;

	textSize(textObj.size);
	textFont(textObj.font);

	width = textWidth(string);
	height = textAscent() + textDescent();

	textObj.text = string.toUpperCase();

	textObj.width = terminalOutput.width;
	textObj.height = ceil(width / textObj.width) * height + height * 0.5;

	terminalOutput.children[textObj.name] = textObj;

	textObj.y = terminalOutput.height;
	textObj.x = terminalOutput.x;


	//TODO: replace this with an animation
	if (terminalOutput.height + textObj.height >= terminalOutputPane.height) {
		alpha = terminalOutputPane.height - terminalOutput.height;

		terminalOutput.rawSetHeight(terminalOutput.height + alpha);
		terminalOutput.y -= textObj.height - alpha;

	} else {
		terminalOutput.rawSetHeight(terminalOutput.height + textObj.height);
	}

	pop();

}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(60);

  loadFromBrowser();

  terminalOutputPane.x = windowWidth * 0.1;
  terminalOutput.y = 0;
  terminalOutputPane.width = windowWidth * 0.8;
  terminalOutputPane.height = windowHeight * 0.8;

  terminalInputPane.x = terminalOutputPane.x;
  terminalInputPane.y = terminalOutputPane.height;
  terminalInputPane.width = terminalOutputPane.width;
  terminalInputPane.height = windowHeight * 0.1;
  terminalArrow.y = terminalInputPane.y;
  terminalArrow.size = terminalInputPane.height ;
  terminalArrow.height = terminalInputPane.height + 1;
  terminalArrow.width = terminalInputPane.height + 1;

  terminalText.x = terminalInputPane.x + terminalInputPane.height;
  terminalText.y = terminalInputPane.y;
  terminalText.size = terminalInputPane.height;
  terminalText.height = terminalInputPane.height;
  terminalText.width = terminalInputPane.width - terminalInputPane.height;


  let textBuffer = "";

  addKeyEventListener(function(keyEvent) {
  	if (keyEvent.key.length === 1 && /^[a-zA-Z0-9.,;:-_\/\'\" ]+$/i.test(keyEvent.key)) {
  		textBuffer += keyEvent.key.toUpperCase();
  	} else if (keyEvent.key === "Backspace") {
  		if (keyEvent.ctrlKey || keyEvent.metaKey) {
  			textBuffer = "";
  		} else {
  			textBuffer = textBuffer.substring(0, textBuffer.length - 1);
  		}
  	} else if (keyEvent.key === "Enter") {
  		textBuffer = handleInput(textBuffer);
  	}

  	terminalText.text = textBuffer;

  	let completion = getAutoComplete(textBuffer);

  	if (completion) {
  		terminalTextCompletion.text = completion.substring(textBuffer.length).toUpperCase();
  	} else {
  		terminalTextCompletion.text = "";
  	}
  });

  setTimeout(start, 500);


  setTimeout(update, 200);
  
}

let choices = [];
let choiceCallbacks = [];

function addChoice(choice, callback) {
	choices.push(choice);
	choiceCallbacks.push(callback);
}

function getSaveString() {
	let s = "";

	for (var p in savePoint) {
		s += "" + (savePoint[p] + 1);
	}

	return (parseInt(s)).toString(36);
}

function loadSaveString(string) {
	let point = [];

	let i = parseInt(string, 36);

	for (var j = 0; j < (i + "").length; j++) {
		point.push(parseInt((i + "").charAt(j)) - 1);
	}

	if (point.includes(undefined) || point.includes(NaN)) {
		printTerm("-COULD NOT LOAD SAVE '" + string + "'!");
		return;
	}

	console.log(i, point);

	savePoint = point;
}

function loadSave(save, part, skipReverse) {

		var save = save || savePoint.slice();

		if (!skipReverse) {
			save.reverse();
		}

		var part = part || story;

		if (typeof part === "function") {
			return loadSave(part(), save);
		} else {
			for (let p in part) {
				if (typeof part[p] === "object"); else continue;

				let choice = save.pop();

				let i = 0;
				for (let c in part[p]) {

					if (i === choice) {
						if (save.length === 0) {
							queue(playStory(part[p][c]));
						} else {
							loadSave(save, part[p][c], true);
						}
						return;
					}

					i++;
				}
			}


			printTerm("COULD NOT LOAD SAVE '" + getSaveString(savePoint) + "'!");
		}
	}

function handleInput(string) {
	if (string === "/SETTINGS") {
		let _choices = choices;
		let _choiceCallbacks = choiceCallbacks;

		choices = [];
		choiceCallbacks = [];

		let q = queuedUpdates;
		queuedUpdates = [];

		queue(() => settings(function() {
			choices = _choices;
			choiceCallbacks = _choiceCallbacks;
			queuedUpdates = q;
		}), true);

		return "";
	} else if (string === "/SAVE") {
		printTerm(getSaveString(), undefined, undefined, true, true);
		return "";

	} else if (string.startsWith("/LOAD ")) {
		loadSaveString(string.substring("/LOAD ".length));
		loadSave();
		return "";
	} else if (string === "/HOME") {
		queue(function() {
			choices = [];
			choiceCallbacks = [];

			start();

		}, true);

		return "";
	}

	var string = string.toLowerCase();

	function choose(n) {
		let f = choiceCallbacks[n];

		console.log(string, n);

		choices = [];
		choiceCallbacks = [];

		f();
	}

	if (choices != []) {

		let matchFound = false;
		let match = null;

		for (var i = 0; i < choices.length; i++) {
			if (choices[i].startsWith(string) || ((i + 1) + ". " + choices[i]).startsWith(string)) {
				if (!matchFound) {
					match = i;
					matchFound = true;
				} else {
					matchFound = false;
					break;
				}
			}
		}


		if (matchFound) {
			printTerm(">" + choices[match]);
			choose(match);
			return "";
		}
	
	}

	return string.toUpperCase();
}

function getAutoComplete(string) {
	var string = string.toLowerCase();

	if (choices != []) {

		let startsWithNumber = false;
		let matchFound = false;
		let match = null;

		for (var i = 0; i < choices.length; i++) {
			if (choices[i].startsWith(string)) {
				if (!matchFound) {
					match = i;
					matchFound = true;
				} else {
					matchFound = false;
					return null;
				}
			}

			if (((i + 1) + ". " + choices[i]).startsWith(string)) {
				if (!matchFound) {
					match = i;
					startsWithNumber = true;
					matchFound = true;
				} else {
					matchFound = false;
					return null;
				}
			}
		}


		if (matchFound) {
			if (!startsWithNumber) {
				return choices[match];
			} else {
				return (match + 1) + ". " + choices[match];
			}
		}
		
	}

	return null;
}


function windowResized() {
  resizeCanvas(windowWidth, windowHeight);

  terminalOutputPane.x = windowWidth * 0.1;
  terminalOutputPane.width = windowWidth * 0.8;
  terminalOutputPane.height = windowHeight * 0.8;

  terminalInputPane.x = terminalOutputPane.x;
  terminalInputPane.y = terminalOutputPane.height;
  terminalInputPane.width = terminalOutputPane.width;
  terminalInputPane.height = windowHeight * 0.1;
  terminalArrow.y = terminalInputPane.y;
  terminalArrow.size = terminalInputPane.height;
  terminalArrow.height = terminalInputPane.height + 1;
  terminalArrow.width = terminalInputPane.height + 1;

  terminalText.x = terminalInputPane.x + terminalInputPane.height;
  terminalText.y = terminalInputPane.y;
  terminalText.size = terminalInputPane.height;
  terminalText.height = terminalInputPane.height;
  terminalText.width = terminalInputPane.width - terminalInputPane.height;
}

function draw() {
	background(backgroundColour);
	PARENT.draw();
}

function queue(f, override) {
	let q = (override ? queueOverride : queuedUpdates);

	q.reverse();
	q.push(f);
	q.reverse();
}

function update() {
	let f = (queueOverride.length === 0 ? queuedUpdates : queueOverride).pop();

	(f || (()=>0))();

	setTimeout(update, updateWaitTime);
	updateWaitTime = 200;
}

keyEventListeners = [];

function addKeyEventListener(listener) {
	keyEventListeners.push(listener);
}

function removeKeyEventListener(listener) {
	let index = keyEventListeners.indexOf(listener);

	if (index > -1) {
		keyEventListeners.splice(index, 1);
	}
}

function keyTyped(keyEvent) {
	for (var i = 0; i < keyEventListeners.length; i++) {
		let listener = keyEventListeners[i];

		if (typeof listener === "function") {
			listener(keyEvent);
		} else {
			listener.keyEvent(keyEvent);
		}
	}
}

function keyPressed(keyEvent) {
	if (keyEvent.key === "Backspace") {
		for (var i = 0; i < keyEventListeners.length; i++) {
		let listener = keyEventListeners[i];

		if (typeof listener === "function") {
			listener(keyEvent);
		} else {
			listener.keyEvent(keyEvent);
		}
	}
	}
}

function start() {
	if (window.localStorage.getItem("ddtg.tutorial") === null) {
		tutorial(0);
		return
	}

	printTerm("1. play")
	printTerm("2. chapters")
	printTerm("3. settings")
	printTerm("4. commands")

	function commands() {
		printTerm("commands:")
		printTerm("/settings - opens the settings menu")
		printTerm("/save - prints the save string")
		printTerm("/load <SaveString> - loads a save string")
		printTerm("/home - exits the game and goes back to the main menu")

		start()
	}

	addChoice("play", () => playStory(story));
	addChoice("chapters", () => chapters(start))
	addChoice("settings", () => settings(start));
	addChoice("commands", commands);
}

function tutorial(phase) {
	if (phase === 0) {
		printTerm("Welcome to my DD typing game.")
		printTerm("this game is set in a fantasy world.")
		printTerm("sometimes you will be presented with a choice.")
		printTerm("to choose one of the options, either enter the number next to the choice, or an unambiguous part of it.")
		printTerm("The choice that will be selected when you hit enter will be displayed as a grey autocompletion.")
		printTerm("1. a choice")
		printTerm("2. another choice")
		printTerm("3. some different choice")
		printTerm("Try it out now!")

		addChoice("a choice", () => tutorial(1))
		addChoice("another choice", () => tutorial(1))
		addChoice("some different choice", () => tutorial(1))
	} else if (phase === 1) {
		printTerm("Good!")
		printTerm("Be careful, what you choose may change the world around you!")

		printTerm("1. end the tutorial")

		addChoice("end the tutorial", function() {window.localStorage.setItem("ddtg.tutorial", true); start()})
	}
}

function chapters(home) {

	let index = 0;

	for (let c in unlockedChapters) {
		printTerm(++index + ". " + c);
		addChoice(c, function() {
			loadSaveString(unlockedChapters[c]);
			loadSave();
		})
	}

	if (index === 0) {
		printTerm("no chapters unlocked");
		home();
		return;
	}

	printTerm(++index + ". back")
	addChoice("back", home);
}

function settings(home) {
	printTerm("choose a setting to change");

	printTerm("1. text speed");
	printTerm("2. text size");
	printTerm("3. back");

	function back(f) {
		return function() {
			f();
			settings(home);
		}
	}

	function speed() {
		printTerm("1. very slow");
		printTerm("2. slow");
		printTerm("3. normal");
		printTerm("4. fast");
		printTerm("5. too fast");
		printTerm("6. back");

		addChoice("very slow", back(() => textSpeed = 75 + textSpeedModifier));
		addChoice("slow", back(() => textSpeed = 50 + textSpeedModifier));
		addChoice("normal", back(() => textSpeed = 18 + textSpeedModifier));
		addChoice("fast", back(() => textSpeed = 12 + textSpeedModifier));
		addChoice("too fast", back(() => textSpeed = 1));
		addChoice("back", () => settings(home));
	}

	function size() {
		printTerm("1. small");
		printTerm("2. normal");
		printTerm("3. large");
		printTerm("4. back");

		addChoice("small", back(() => printSize = 15));
		addChoice("normal", back(() => printSize = 17));
		addChoice("large", back(() => printSize = 20));
		addChoice("back", () => settings(home));
	}

	addChoice("text speed", speed);
	addChoice("text size", size);
	addChoice("back", home);
}

function playStory(part) {

	saveToBrowser();

	let lastFunc = () => null
	let func = null;

	if (typeof part === "object") {
		for (let n in part) {
			let p = part[n];
			if (typeof p === "string") {
				if (p.startsWith("//")) {
					unlockedChapters[p.substring(2)] = getSaveString();
				} else {
					printTerm(p);
				}
			} else if (typeof p === "object") {
					
				queue(function() {
					updateWaitTime = 1;

					choices = [];
					choiceCallbacks = [];

					let i = 0;
					for (let c in p) {
						console.log(c, p, p[c]);
						let index = i + 0;
						


						addChoice(c, function() {
							savePoint.push(index);


							console.log(savePoint);

							playStory(p[c]);
						});
						printTerm("    " + (++i) + ". " + c + (p[c].length === 0 ? " (NO CONTENT BEYOND THIS POINT)" : ""), undefined, undefined, true);
					}
				});

				return;
			}
		}

		printTerm("save point: " + getSaveString(), null, null, true, false);
		queue(start, true);
	} else if (typeof part === "function") {
		func = () => playStory(part());
	}

	queue(func);
}


function saveToBrowser() {
	window.localStorage.setItem("ddtg.unlockedChapters", JSON.stringify(unlockedChapters));
	window.localStorage.setItem("ddtg.textSize", printSize);
	window.localStorage.setItem("ddtg.textSpeed", textSpeed);
}

function loadFromBrowser() {
	unlockedChapters = JSON.parse(window.localStorage.getItem("ddtg.unlockedChapters")) || unlockedChapters;
	printSize = parseInt(window.localStorage.getItem("ddtg.textSize")) || printSize;
	textSpeed = parseInt(window.localStorage.getItem("ddtg.textSpeed")) || textSpeed;
}

