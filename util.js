function wait(t) {
  return new Promise((r, j) => setTimeout(r, t));
}


var GameObject = function(name, drawFunc, parent) {
  this.children = {};

  let width = 1;
  let height = 1;
  this.hide = false;

  drawFunc = drawFunc || function() {
    if (this.texture) {
      image(this.texture, this.x, this.y, this.width, this.height);
    }
  }

  let x = 0;
  let y = 0;

  this.getX = function() {
    return x;
  }

  this.getY = function() {
    return y;
  }

  this.setX = function(newX) {
    var oldX = x;
    x = newX;

    for (var child in this.children) {
      this.children[child].setX(newX + this.children[child].getX() - oldX);
    }

    return newX;
  }

  this.setY = function(newY) {
    var oldY = y;
    y = newY;

    for (var child in this.children) {
      this.children[child].setY(newY + this.children[child].getY() - oldY);
    }

    return newY;
  }

  this.addX = function(a) {
    this.setX(this.getX() + a);
  }

  this.addY = function(a) {
    this.setY(this.getY() + a);
  }

  this.rawSetWidth = function(v) {
    width = v;
  }

  this.rawSetHeight = function(v) {
    height = v;
  }

  Object.defineProperty(this, "x", {get: this.getX, set: this.setX});
  Object.defineProperty(this, "y", {get: this.getY, set: this.setY});

  Object.defineProperty(this, "width", {
    get: function() {
      return width;
    },
    set: function(v) {
      let ratio = v / width;

      width = v;

      for (var child in this.children) {
        this.children[child].width *= ratio;
      }
    }
  });

  Object.defineProperty(this, "height", {
    get: function() {
      return height;
    },
    set: function(v) {
      let ratio = v / height;

      height = v;

      for (var child in this.children) {
        this.children[child].height *= ratio;
      }
    }
  });

  Object.defineProperty(this, "name", {get: (() => name), set: ((o) => name)});
  Object.defineProperty(this, "draw", {get: (() => () => {
    if (!this.hide) {
      drawFunc.apply(this);
      for (var child in this.children) {
        this.children[child].draw();
      }
    }
  }), set: ((f) => {drawFunc = f; return this.draw;})});

  parent = parent || window["PARENT"];

  if (parent !== undefined) {
    parent.children[name] = this;
  }

}

const PARENT = new GameObject("PARENT");

PARENT.draw = () => null;

window["PARENT"] = PARENT;

function Text(name, drawFunc, parent) {
  function draw() {

    if (this.text !== undefined) {
      window.textFont(this.font);
      window.textSize(this.size);
      window.textAlign(this.horizAlign, this.vertAlign);
      
      window.fill(this.fill);

      if (this.width !== 0 && this.height !== 0) {
        window.text(this.text, this.x, this.y, this.width, this.height);
      } else {
        window.text(this.text, this.x, this.y);
      }

    }
  }

  GameObject.apply(this, [name, function() { if (drawFunc) drawFunc.apply(this); draw.apply(this); }, parent]);

  this.text = "";
  this.font = "Courier New";
  this.size = 17;
  this.width = 0;
  this.height = 0;
  this.vertAlign = 'top';
  this.horizAlign = 'left';
  this.fill = 256;

  
}